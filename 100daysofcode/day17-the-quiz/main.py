from question_model import Question
from data import populate_question_database
from quiz_brain import QuizBrain

question_data = populate_question_database()
question_bank = []
for question_entry in question_data:
    question_bank.append(Question(question_entry["question"], question_entry["correct_answer"]))

quiz = QuizBrain(question_bank)

while quiz.still_has_questions():
    quiz.next_question()

print("You've completed the quiz.")
print(f"Your final score was {quiz.score}/{quiz.question_number}")