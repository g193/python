from clear import clear
from art import logo

print(logo)
print("Welcome to the silent auction bid system. Place first bid.")

bids_placed = {}

def place_a_bid():
  name = input("Name: ")
  bid = int(input("Bid: "))
  bids_placed[name] = bid

def find_winner(bids_placed):
  max = 0
  winner = ""
  for bidder in bids_placed:
    if int(bids_placed[bidder]) > max:
      max = bids_placed[bidder]
      winner = bidder
  return winner

go_again = True
while go_again:
  place_a_bid()
  choice = input("Anyone else wants to bid? Type 'yes' or 'no'")
  if choice == "yes":
    clear()
  elif choice == "no":
    go_again = False

print(f"The winner is... {find_winner(bids_placed)}")
