def divide(list, start, end):

    i = start
    border_value = list[end]

    for j in range(start, end):
        if list[j] <= border_value:
            list[i], list[j] = list[j], list[i]
            i += 1

    list[i], list[end] = list[end], list[i]
    return i

def quick_select(list, start, end, k):

    if k >= len(list):
        return None

    if start <= end:

        border_index = divide(list, start, end)

        if border_index == k:
            return list[border_index]
        
        elif border_index > k:
            quick_select(list, start, border_index-1, k)
        
        else:
            quick_select(list, border_index+1, end, k)