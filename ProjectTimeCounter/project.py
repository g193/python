from datetime import datetime, timedelta

class Project():
    
    project_name: str
    start_time: datetime
    time_spent: timedelta

    def __init__(self, name: str):
        self.project_name: str = name
        self.time_spent: timedelta = timedelta(0)

    def start_timer(self):
        self.start_time = datetime.now()

    def stop_timer(self):
        self.time_spent += datetime.now() - self.start_time

    def get_time_spent(self) -> str:
        return str(self.time_spent).split(".")[0]
