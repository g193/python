import logging
import pytest

def test_logging():
    logger = logging.getLogger(__name__)

    fileHandler = logging.FileHandler("logfile.log")

    formatter = logging.Formatter("%(asctime)s :%(levelname)s :%(name)s :%(message)s")
    fileHandler.setFormatter(formatter)
    fileHandler.setLevel(logging.INFO)

    logger.addHandler(fileHandler)

    logger.debug("A debug statement is executed")
    logger.error("An error statement is executed")
    logger.warning("A warning statement is executed")