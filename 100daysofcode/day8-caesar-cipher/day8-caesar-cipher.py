from logo import logo
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

#User input
print(logo)

#Both encode and decode combined into one.
def caesar(input_text, shift_amount, direction):
    output_text = ""
    if direction == "decode":
        shift_amount *= -1 #Reverse the shift direction
    for letter in input_text:
        if letter in alphabet:
            position = alphabet.index(letter)
            #Modulo is used here so we never get index out of range error
            new_position = (position + shift_amount) % len(alphabet)
            output_text += alphabet[new_position]
        else:
            output_text += letter
    print(f"The {direction}d text is {output_text}")

#Execution part - menu and user input
go_again = True
while go_again:
    direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n")
    text = input("Type your message:\n").lower()
    shift = int(input("Type the shift number:\n"))
    if direction == "encode" or "decode":
        caesar(text, shift, direction)
    else:
        print("You have to choose the correct option")
    print("Type 'yes' if you want to go again. Type 'no' to exit.")
    choice = input()
    if choice == "no":
        go_again = False
