from art import logo
print(logo)

#Add
def add(n1, n2):
  return n1 + n2

#Substract
def substract(n1, n2):
  return n1 - n2

#Multiply
def multiply(n1, n2):
  return n1 * n2

#Divide
def divide(n1, n2):
  return n1 / n2

#Available operations
operations = {
  "+": add,
  "-": substract,
  "*": multiply,
  "/": divide
}

def calculator():
  num1 = float(input("What's the first number?: "))

  go_again = True

  while go_again:
    for symbol in operations:
      print(symbol)
    operation_symbol = input("Pick an operation you want to perform: ")
    num2 = float(input("What's the next number?: "))
    result = operations[operation_symbol](num1, num2)
    print(f"{num1} {operation_symbol} {num2} = {result}")
    if input("Type 'y' to chain another calculation, or type 'n' to start a new calculation ") == "y":
      num1 = result
    else:
      go_again = False
      calculator()

calculator()
