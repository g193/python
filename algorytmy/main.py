from Sorting.BubbleSort import bubble_sort
from Sorting.InsertSort import insert_sort
from Sorting.SelectSort import select_sort
from Sorting.MergeSort import merge_sort

from Searching.LinearSearch import linear_search
from Searching.BinarySearch import binary_search

ARRAY = [1, 3, 9, 2, 0, 6, 4, 7, 5, 3]
SORTED_ARRAY = [0, 1, 2, 3, 3, 4, 5, 6, 7, 9]

#sorted_array = merge_sort(ARRAY)
#print(sorted_array)

index = binary_search(22, SORTED_ARRAY)
print(index)