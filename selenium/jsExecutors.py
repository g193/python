from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By


#CHROME OPTIONS - uwaga - aby działało muszę przekazać ten obiekt przy tworzeniu drivera
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("headless")
# można też dodać inne ciekawe opcje jak ignorowanie ostrzeżeń z certyfikatami
chrome_options.add_argument("--ignore-certificate-errors")
"""
inne ciekawe opcje
--start-maximized
--start-fullscreen
"""



service_obj = Service("/home/krzysztof/Repozytorium/python/selenium/chromedriver")
# tutaj przekazuję obiekt opcji 
driver = webdriver.Chrome(service=service_obj, options=chrome_options)
driver.get("https://rahulshettyacademy.com/AutomationPractice/")

#JS EXECUTORS
driver.execute_script("window.scrollBy(0, document.body.scrollHeight);")
driver.execute_script("window.scrollBy(0, -500);")

#SCREENSHOT
driver.get_screenshot_as_file("selenium/screen.png")