import random
from art import logo
print(logo)
#For purpose of this game the suit of the card does not matter
cards = [
11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10,
11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10,
11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10,
11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10
]

#Create empty lists for both players and dealers hand
player_cards = []
dealer_cards = []

def print_cards():
    print(f"Dealers cards: {dealer_cards} Total: {dealer_score}")
    print(f"Players cards: {player_cards} Total: {player_score}")

#Function that will pick a random card, assign it to a player and remove this card from deck
def deal_card(who):
    card = random.choice(cards)
    who.append(card)
    cards.remove(card)

def calculate_score(whose_cards):
    if len(whose_cards) == 2 and sum(whose_cards) == 21:
        return 0
    if 11 in whose_cards and sum(whose_cards) > 21:
        whose_cards.remove(11)
        whose_cards.append(1)
    return sum(whose_cards)

def compare(user_score, computer_score):
    if user_score == computer_score:
        return "It's a draw"
    elif user_score == 0:
        return "You win! You have a Blackjack!"
    elif computer_score == 0:
        return "You lose. Dealer has a Blackjack!"
    elif user_score > 21:
        return "You lose."
    elif computer_score > 21:
        return "You win!"
    elif user_score > computer_score:
        return "You win!"
    else:
        return "You lose."

#Game starts here
for turn in range(2):
    deal_card(dealer_cards)
    deal_card(player_cards)
player_score = calculate_score(player_cards)
dealer_score = calculate_score(dealer_cards)
print_cards()

game_over = False

#User turn loop
while not game_over:
    player_score = calculate_score(player_cards)
    dealer_score = calculate_score(dealer_cards)
    if player_score == 0 or dealer_score == 0 or player_score > 21:
        game_over = True
    else:
        choice = input("Do you want another card? Type 'h' to hit again or 's' to stand: ")
        if choice == "h":
            deal_card(player_cards)
            player_score = calculate_score(player_cards)
            print_cards()
        else:
            game_over = True

#Dealer turn loop
while dealer_score != 0 and dealer_score < 17 and player_score <= 21:
    print("Dealer gets another card.")
    deal_card(dealer_cards)
    dealer_score = calculate_score(dealer_cards)
    print_cards()


print(compare(player_score, dealer_score))
