from tkinter import *
from tkinter import messagebox
import password_generator
import pyperclip

# ---------------------------- PASSWORD GENERATOR ------------------------------- #
def generate_password():
    password = password_generator.generate()
    entry_password.delete(0, END)
    entry_password.insert(0, password)
    pyperclip.copy(password)

# ---------------------------- SAVE PASSWORD ------------------------------- #
def save_password():
    website = entry_website.get()
    email = entry_email.get()
    password = entry_password.get()

    if not website or not password:
        messagebox.showinfo(title="Oops", message="Please fill out all form fields")
    else:    
        is_ok = messagebox.askyesno(title=website, message=f"These are the datails entered: \n"
                                                   f"Email: {email} \n"
                                                  f"Password: {password} \n"
                                                  "Do you want to save this data?")
        if is_ok:
            with open('password_database', "a") as file:
                file.write(f"{website} | {email} | {password} \n")
            entry_website.delete(0, END)
            entry_password.delete(0, END)
# ---------------------------- UI SETUP ------------------------------- #

window = Tk()
window.title("Password Manager")
window.config(padx=20, pady=20)

canvas = Canvas(height=200, width=200)
logo_img = PhotoImage(file="logo.png")
canvas.create_image(100, 100, image=logo_img)
canvas.grid(row=0, column=1)

#Labels
label_website = Label(text="Website:")
label_website.grid(row=1, column=0)
label_email = Label(text="Email/Username:")
label_email.grid(row=2, column=0)
label_password = Label(text="Password:")
label_password.grid(row=3, column=0)

#Entries
entry_website = Entry(width=55)
entry_email = Entry(width=55)
entry_password = Entry(width=36)

entry_website.grid(row=1, column=1, columnspan=2)
entry_email.grid(row=2, column=1, columnspan=2)
entry_password.grid(row=3, column=1)

entry_email.insert(0, "fake_mail@gmail.com")

#Buttons
button_generate_pass = Button(text="Generate password", command=generate_password)
button_generate_pass.grid(row=3, column=2) 
button_add = Button(text="Add", width=36, command=save_password)
button_add.grid(row=4, column=1) 



window.mainloop()
