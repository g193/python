from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By

service_obj = Service("/home/krzysztof/Repozytorium/python/selenium/chromedriver")
driver = webdriver.Chrome(service=service_obj)
driver.get("https://rahulshettyacademy.com/AutomationPractice/")

# CHECKBOX 

#driver.find_element(By.ID, "checkBoxOption2").click()

checkboxes = driver.find_elements(By.XPATH, "//input[@type='checkbox']")
for element in checkboxes:
    if element.get_attribute("value") == "option2":
        element.click()
        assert element.is_selected()
        break


# RADIOBUTTON
radiobuttons = driver.find_elements(By.CSS_SELECTOR, ".radioButton")
radiobuttons[2].click()
assert radiobuttons[2].is_selected()

assert driver.find_element(By.ID, "displayed-text").is_displayed()
driver.find_element(By.ID, "hide-textbox").click()
assert not driver.find_element(By.ID, "displayed-text").is_displayed() 

