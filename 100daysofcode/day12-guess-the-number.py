#Guess the number game
import random

#Print welcome message and simple menu
print("Welcome to the Guess The Number game! You have to guess the number from 1 to 100")
print("Choose difficulty level:")
print("1 - Easy")
print("2 - Hard")

#Ask for difficulty level and assing correct number of chances to guess
difficulty = input()
if difficulty == "1":
    chances = 6
if difficulty == "2":
    chances = 8

#Get a random number
the_number = random.randint(1,100)

#Flag for win situation
is_game_over = False

#Main loop
while chances > 0 and is_game_over == False:
    guess = int(input("Type your guess:"))
    if guess == the_number:
        print("You win!")
        is_game_over = True
    elif guess < the_number:
        print("To low")
        chances -= 1
    elif guess > the_number:
        print("To high")
        chances -= 1
    print(f"You have {chances} chances left")
