from turtle import Turtle
ALIGNMENT = "center"
FONT = ("Noto Mono", 16, "bold")

class Scoreboard(Turtle):

    def __init__(self):
        super().__init__()
        self.color("white")
        self.penup()
        self.hideturtle()
        self.goto(0, 250)
        self.l_score = 0
        self.r_score= 0
        self.update_scoreboard()

    def increase_score_left(self):
        self.l_score += 1
        self.update_scoreboard()

    def increase_score_right(self):
        self.r_score += 1
        self.update_scoreboard()

    def update_scoreboard(self):
        self.clear()
        self.write(f"{self.l_score} : {self.r_score}",
                   align=ALIGNMENT,
                   font=FONT
                   )