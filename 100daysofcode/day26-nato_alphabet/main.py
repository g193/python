from numpy import outer
import pandas

data = pandas.read_csv("nato_phonetic_alphabet.csv")
data_dict = {row.letter: row.code for (index, row) in data.iterrows()}

word = input("Enter a word: ").upper()
try:
    output = [data_dict[letter] for letter in word]
except KeyError:
     print("Sorry, only letters in the alphabet please.")
else:
    print(output)