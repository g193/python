print('''
*******************************************************************************
          |                   |                  |                     |
 _________|________________.=""_;=.______________|_____________________|_______
|                   |  ,-"_,=""     `"=.|                  |
|___________________|__"=._o`"-._        `"=.______________|___________________
          |                `"=._o`"=._      _`"=._                     |
 _________|_____________________:=._o "=._."_.-="'"=.__________________|_______
|                   |    __.--" , ; `"=._o." ,-"""-._ ".   |
|___________________|_._"  ,. .` ` `` ,  `"-._"-._   ". '__|___________________
          |           |o`"=._` , "` `; .". ,  "-._"-._; ;              |
 _________|___________| ;`-.o`"=._; ." ` '`."\` . "-._ /_______________|_______
|                   | |o;    `"-.o`"=._``  '` " ,__.--o;   |
|___________________|_| ;     (#) `-.o `"=.`_.--"_o.-; ;___|___________________
____/______/______/___|o;._    "      `".o|o_.--"    ;o;____/______/______/____
/______/______/______/_"=._o--._        ; | ;        ; ;/______/______/______/_
____/______/______/______/__"=._o--._   ;o|o;     _._;o;____/______/______/____
/______/______/______/______/____"=._o._; | ;_.--"o.--"_/______/______/______/_
____/______/______/______/______/_____"=.o|o_.--""___/______/______/______/____
/______/______/______/______/______/______/______/______/______/______/_____ /
*******************************************************************************
''')
print("Welcome to Treasure Island.")
print("Your mission is to find the treasure.") 

print("Where do you want to go? You can go 'left' or 'right'")
choice1 = input()
if (choice1 == "left"):
  print("You see a lake, there is an island in the middle of the lake. You can 'wait' for a boat or try to 'swim' there instead.")
  choice2 = input()
  if (choice2 == "swim"):
    print("Oh no! You are attacked by a giant trout and dia a terrible death. Game over.")
  elif (choice2 == "wait"):
    print("You take the boat and arrive at the island. There is an old temple with three entrances. Which door will you choose? 'red', 'blue' or 'yellow'?")
    choice3 = input()
    if (choice3 == "red"):
      print("You walk slowly inside the dark corridor. The door slams shut behind you. You see a flame at the end of the corridor. Oh no! There are flamethrowers hidden inside the walls! You burn and die. Game over.")
    elif (choice3 == 'blue'):
      print("Oh no! You stepped right into a bears nest! The bear attacs you and you die. Game over.")
    elif (choice3 == 'yellow'):
      print("There is a treasure chest inside! You won the game!")
    else: 
      print("Wrong command, you die. Game over.")
else:
  print("Oh no! You fall into a hole and die. Game over.")  
