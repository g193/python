from menu import *
from coffee_maker import CoffeeMaker
from money_machine import MoneyMachine


def prompt():
    """Asks for user input"""
    items = menu.get_items()
    user_choice = input(f"What would you like? {items}: ")
    if user_choice == "off":
        return user_choice
    elif user_choice == "report":
        return user_choice
    else:
        return menu.find_drink(user_choice)


# Create objects
coffee_machine = CoffeeMaker()
money_machine = MoneyMachine()
menu = Menu()


# Declare variables
power_status = True
money = 0


while power_status:
    choice = prompt()
    if choice == "off":
        power_status = False
    elif choice == "report":
        print(coffee_machine.report())
        print(money_machine.report())
    else:
        print(f"Your drink will cost {choice.cost} ")
        resource_status = (coffee_machine.is_resource_sufficient(choice))
        if resource_status:
            transaction_status = money_machine.make_payment(choice.cost)
            if transaction_status > 0:
                coffee_machine.make_coffee(choice)
