import pytest
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By

pytest.mark.usefixtures("setup")
class BaseClass():
    
    def verifyLinkPresence(self, text):

        #EXPLICIT WAIT
        wait = WebDriverWait(self.driver, 10)
        wait.until(expected_conditions.presence_of_element_located((By.LINK_TEXT, text)))