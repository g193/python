from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time

service_obj = Service("/home/krzysztof/Repozytorium/python/selenium/chromedriver")
driver = webdriver.Chrome(service=service_obj)
driver.get("https://rahulshettyacademy.com/dropdownsPractise/")

driver.find_element(By.ID, "autosuggest").send_keys("ind")
time.sleep(2)
elements = driver.find_elements(By.CSS_SELECTOR, "li[class='ui-menu-item' a")

for element in elements:
    if element.text == "India":
        element.click()
        break

assert driver.find_element(By.ID,"autosuggest").get_attribure("value") == "India"