from selenium.webdriver.common.by import By


class CheckoutPage:
    def __init__(self, driver):
        self.driver = driver

    cardTitle = (By.CSS_SELECTOR, ".card-title a")
    cardFooter = (By.CSS_SELECTOR, ".card-footer button")
    card = (By.CSS_SELECTOR, ".card")
    checkOut = (By.CSS_SELECTOR, ".btn-success")

    def getCardTitles(self):
        return self.driver.find_elements(*CheckoutPage.cardTitle)

    def getCardFooter(self):
        return self.driver.find_elements(*CheckoutPage.cardFooter)

    def getCards(self):
        return self.driver.find_elements(*CheckoutPage.card)

    def getCheckoutItems(self):
        return self.driver.find_element(*CheckoutPage.checkOut)
