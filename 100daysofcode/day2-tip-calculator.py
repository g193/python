print("Welcome to the tip calculator.")
total_bill = float(input("What was the total bill? "))
tip_percent = int(input("What tip percentage would you like to give? 10, 12 or 15? "))
people = int(input("How many people to split the bill? "))

result = round((total_bill / people) * (tip_percent/100 + 1),2)
print(f"Each person should pay: {result}")
