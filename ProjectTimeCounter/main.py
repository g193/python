from project import Project
import time

project: Project = Project("first")

project.start_timer()

time.sleep(3)

project.stop_timer()

duration = project.get_time_spent()

print(duration)
print(type(duration))
