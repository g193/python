from turtle import Screen
from paddle import Paddle
from ball import Ball
from scoreboard import Scoreboard
import time

screen = Screen()
screen.bgcolor("black")
screen.setup(width=800, height=600)
screen.title("Pong")
screen.tracer(0)

l_paddle = Paddle("left")
r_paddle = Paddle("right")
ball = Ball()
scoreboard = Scoreboard()

screen.listen()
screen.onkey(l_paddle.go_up, "w")
screen.onkey(l_paddle.go_down, "s")

screen.onkey(r_paddle.go_up, "Up")
screen.onkey(r_paddle.go_down, "Down")

score_left = 0
score_right = 0

game_is_on = True
while game_is_on:
    screen.update()
    ball.move()
    time.sleep(ball.speed)

    if (ball.ycor() > 280) or (ball.ycor() < -280):
        ball.bounce_y()

    if (ball.distance(r_paddle) < 50 and ball.xcor() > 320):
        ball.bounce_x()
    elif (ball.distance(l_paddle) < 50 and ball.xcor() < -320):
        ball.bounce_x()

    if (ball.xcor() > 380):
        scoreboard.increase_score_left()
        ball.reset()
    elif (ball.xcor() < -380):
        scoreboard.increase_score_right()
        ball.reset()

screen.exitonclick()
