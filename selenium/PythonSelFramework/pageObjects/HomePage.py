from selenium.webdriver.common.by import By
from CheckoutPage import CheckoutPage

class HomePage:
    def __init__(self, driver):
        self.driver = driver

    shop = (By.LINK_TEXT, "Shop")

    def shopItems(self):
        self.driver.find_element(*HomePage.shop).click()
        return CheckoutPage(self.driver)
