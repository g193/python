def interpolation_search(value, list):

    start = 0
    end = len(list) - 1
    found = False

    while start <= end and value >= list[start] and value <= list[end]:

        # warunek konieczny aby później nie wystąpiło dzielenie przez zero
        if list[start] != list[end]:
            mid = start + int( (float(end - start) / (list[end] - list[start])) * (value - list[start]) )

        else:
            mid = start

        if list[mid] == value:
            print(f"Found on position {mid}")
            found = True
            break
        else: 
            if value < list[mid]:
                end = mid - 1
            else:
                start = mid + 1
    
    if found:
        return mid
    else: 
        return None
