import random
from hangman_words import word_list
from hangman_art import *
from clear import clear

end_of_game = False
#!--chosen_word = random.choice(word_list)
#Show game logo
print(logo)
#Choose game mode menu
print("If you want to choose your own word type '0' \n Type anything else to play in single player mode")
game_mode = int(input())
#Get random word from list for singleplayer or type your own word for multiplayer
if game_mode == 0:
    chosen_word = input("The word you choose: ")
    clear()
else:
    chosen_word = random.choice(word_list)

#Create a variable called 'lives' to keep track of the number of lives left.
#Set 'lives' to equal 6.
lives = 6
word_length = len(chosen_word)

#Create blanks
display = []
for _ in range(word_length):
    display.append("_")

while not end_of_game:
    guess = input("Guess a letter: ").lower()
    clear()
    if guess in display:
      print(f"You've already guessed {guess}")
    #Check guessed letter
    for position in range(word_length):
        letter = chosen_word[position]
        if letter == guess:
            display[position] = letter

    #If guess is not a letter in the chosen_word,
    #Then reduce 'lives' by 1.
    #If lives goes down to 0 then the game should stop and it should print "You lose."
    if guess not in chosen_word:
      print(f"Letter {guess} is not in the word. You lose a life.")
      lives -= 1
      if lives == 0:
        end_of_game = True
        print("You lose!")
        print(f"The word was... {chosen_word}")
    #Join all the elements in the list and turn it into a String.
    print(f"{' '.join(display)}")

    #Check if user has got all letters.
    if "_" not in display:
        end_of_game = True
        print("You win!")

    #print the ASCII art from 'stages' that corresponds to the current number of 'lives' the user has remaining.
    print(stages[lives])
