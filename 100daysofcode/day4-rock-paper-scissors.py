import random

rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

choice = int(input("What do you choose? Type 0 for Rock, 1 for Paper and 2 for Scissors."))

computer_choice = random.randint(0,2)
hands = [rock, paper, scissors]

if (choice in range (0,2)):
  print(hands[choice])
  print("Computer chose:")
  print(hands[computer_choice])

  if (choice == 0 and computer_choice == 0):
    print("It's a draw")
  elif (choice == 0 and computer_choice == 1):
    print("You lose")
  elif (choice == 0 and computer_choice == 2):
    print("You win")
  if (choice == 1 and computer_choice == 0):
    print("You win")
  elif (choice == 1 and computer_choice == 1):
    print("It's a draw")
  elif (choice == 1 and computer_choice == 2):
    print("You lose")
  if (choice == 2 and computer_choice == 0):
    print("You lose")
  elif (choice == 2 and computer_choice == 1):
    print("You win")
  elif (choice == 2 and computer_choice == 2):
    print("It's a draw")
else:
  print("You have to choose the right number")
